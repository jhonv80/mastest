﻿using Sol.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Sol.Business
{
    public class EmployeeBusiness
    {

        public List<Employee> getEmployees()
        {
            List<Employee> listEmployees = new List<Employee>();
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://masglobaltestapi.azurewebsites.net/api/Employees");
                request.Method = "GET";
                String test = String.Empty;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    test = reader.ReadToEnd();
                    reader.Close();
                    dataStream.Close();
                }

                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(test)))
                {
                    // Deserialization from JSON  
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(List<Employee>));
                    listEmployees = (List<Employee>)deserializer.ReadObject(ms);
                   
                }
            }
            catch(Exception ex)
            {
            }
            return listEmployees;
        }
    }
}
