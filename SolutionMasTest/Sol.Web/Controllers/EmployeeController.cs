﻿using Sol.Business;
using Sol.Entities;
using Sol.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sol.Web.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            EmployeeBusiness objEmployeeBusiness = new EmployeeBusiness();

            var model = new EmployeeModel()
            {
                listEmployee = objEmployeeBusiness.getEmployees()
            };

            return PartialView("~/Views/Employee/_List.cshtml", model);
        }
    }
}