﻿using Sol.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol.Web.Models
{
    public class ProductModels
    {
        public List<Product> listProducts;
        public Dictionary<string,string> listKindOfStorages;

        public ProductModels()
        {
            listProducts = new List<Product>();
            listKindOfStorages = new Dictionary<string, string>() {
                { "BD", "BD" },
                { "InMemory", "InMemory"}
            };
        }
    }
}