﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sol.Web.Startup))]
namespace Sol.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
