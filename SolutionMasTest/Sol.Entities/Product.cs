﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol.Entities
{
    public class Product
    {
        public int number { get; set; }
        public string title { get; set; }
        public double? price { get; set; }
    }
}
